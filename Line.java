/**
 * Sample Line Problem. This exercise is similar to previous exercise Linea
 * You must obtain minimum 75%
 * 
 * @author Ejercicio: (Milton Jesús Vera Contreras - miltonjesusvc@ufps.edu.co)
 * @author Desarrollador: (Jose Florez - florezjoserodolfo@gmail.com) & (Antoni XXX)
 *
 * @version 0.000000000000001 :) --> Math.sin(Math.PI-Double.MIN_VALUE)
 */
public class Line {

    /**Coordenada X del punto mas a la izquierda o punto inicial*/
    double startX;
    /**Coordenada Y del punto mas a la izquierda o punto inicial*/
    double startY;
    /**Coordenada X del punto mas a la derecha o punto final*/
    double endX;
    /**Coordenada Y del punto mas a la derecha o punto final*/
    double endY;

    /**Default Constructor*/
    public Line() {
        //COMPLETE   
    }

    /**
     * Constructor con parametros.
     * Debe ordenar las coordenadas recibidas
     */
    public Line(double x1, double y1, double x2, double y2) {
        //COMPLETE
        if(x1 == x2) {
            if(y1 <= y2) {
                this.startX = x1;
                this.startY = y1;
                this.endX = x2;
                this.endY = y2;
            }else {
                this.startX = x2;
                this.startY = y2;
                this.endX = x1;
                this.endY = y1;

            }
        } else if(x1 < x2) {
            this.startX = x1;
            this.startY = y1;
            this.endX = x2;
            this.endY = y2;
        } else {
            this.startX = x2;
            this.startY = y2;
            this.endX = x1;
            this.endY = y1;
        }
    }

    /**Retorna true si la linea se encuentra en el primer cuadrante*/
    public boolean isOnFirstQuadrant() {
        if ( (this.getStartX() > 0 && this.getStartY() > 0) || (this.getEndX() > 0 && this.getEndY() > 0) ) return true;
        
        if(this.getStartX() == 0 && this.getStartY() == 0)
            if((this.getEndY() == 0 && this.getEndX() > 0) || (this.getEndX() == 0 && this.getEndY()>0))
                return true;
        
        if(this.getIntersectY() > 0 && this.isOnSecondQuadrant() && this.isOnFourthQuadrant()) {
            return true;
        }
        return false;
    }//fin isOnFirstQuadrant

    /**Retorna true si la linea se encuentra en el segundo cuadrante*/
    public boolean isOnSecondQuadrant() {
        if((this.getStartX() < 0 && this.getStartY() > 0) || (this.getEndX() < 0 && this.getEndY() > 0)) return true;
        
        if(this.getStartX() == 0 && this.getStartY() == 0)
            if(this.getEndY() == 0 && this.getEndX() < 0)
                return true;
                
        if(this.getIntersectY() > 0 && this.isOnFirstQuadrant() && this.isOnThirdQuadrant()) return true;
        return false;
    }//fin isOnSecondQuadrant

    /**Retorna true si la linea se encuentra en el tercer cuadrante*/
    public boolean isOnThirdQuadrant() {
        if((this.getStartX() < 0 && this.getStartY() < 0) || (this.getEndX() < 0 && this.getEndY() < 0)) return true;
        if(this.getIntersectY() < 0 && this.isOnSecondQuadrant() && this.isOnFourthQuadrant()) return true;
        return false;
    }//fin isOnThirdQuadrant

    /**Retorna true si la linea se encuentra en el cuarto cuadrante*/
    public boolean isOnFourthQuadrant() {
        if((this.getStartX() > 0 && this.getStartY() < 0) || (this.getEndX() > 0 && this.getEndY() < 0)) return true;
        if(this.getStartX() == 0 && this.getStartY() == 0)
            if(this.getEndY() == 0 && this.getEndX() < 0)
                return true;
        if(this.getIntersectY() < 0 && this.isOnFirstQuadrant() && this.isOnThirdQuadrant()) return true;
        return false;
    }//fin isOnFourthQuadrant

    /**Retorna true si la linea se encuentra en el primer cuadrante*/
    public int getQuadrants() {
        int n = 0;
        if(this.isOnFirstQuadrant()){
            n++;
        }
        if(this.isOnSecondQuadrant()){
            n++;
        }
        if(this.isOnThirdQuadrant()){
            n++;
        }
        if(this.isOnFourthQuadrant()){
            n++;
        }
        return n;//complete
    }//fin getQuadrants

    /**Mueve la linea horizontalmente hacia la derecha*/
    public void moveRight(int d){
        //COMPLETE
        this.startX += d;
        this.endX += d;
    }

    /**Mueve la linea horizontalmente hacia la izquierda*/
    public void moveLeft(int d){
        //COMPLETE
        this.startX -= d;
        this.endX -= d;
    }

    /**Mueve la linea verticalmente hacia arriba*/
    public void moveUp(int d){
        //COMPLETE 
        this.startY += d;
        this.endY += d;
    }

    /**Mueve la linea verticalmente hacia abajo*/
    public void moveDown(int d){
        //COMPLETE
        this.startY -= d;
        this.endY -= d;
    }

    //Start GetterSetterExtension Code
    /**Getter method startX*/
    public double getStartX(){
        return this.startX;
    }//end method getStartX

    /**Setter method startX*/
    public void setStartX(double startX){
        this.startX = startX;
    }//end method setStartX

    /**Getter method startY*/
    public double getStartY(){
        return this.startY;
    }//end method getStartY

    /**Setter method startY*/
    public void setStartY(double startY){
        this.startY = startY;
    }//end method setStartY

    /**Getter method endX*/
    public double getEndX(){
        return this.endX;
    }//end method getEndX

    /**Setter method endX*/
    public void setEndX(double endX){
        this.endX = endX;
    }//end method setEndX

    /**Getter method endY*/
    public double getEndY(){
        return this.endY;
    }//end method getEndY

    /**Setter method endY*/
    public void setEndY(double endY){
        this.endY = endY;
    }//end method setEndY

    public double getPendient() {
        /*complete*/
        double m = (this.getEndY() - this.getStartY()) / (this.getEndX() - this.getStartX());
        return m;
    }

    public double getIntersectY() {
        double y = this.getStartY() - this.getPendient() * getStartX();
        return y;/*complete*/
    }

    //End GetterSetterExtension Code
    //!
}
